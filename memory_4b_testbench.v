/*	Test bench for 4b memory	*/

module memory_4b_test;
	
	reg t_clk;
	reg t_we;
	reg t_reset;
	reg [1:0]t_addr;
	reg [7:0]t_data;
	wire [7:0]t_out;

 memory_4b m4 (.dout(t_out),
		.clk(t_clk),
		.reset(t_reset),
		.addr(t_addr),
		.din(t_data),
		.we(t_we));

initial begin
	t_clk = 0;
	t_reset = 0;
	t_we = 1;
end

always #5 t_clk = ~t_clk;

initial begin


//Load data into all memory locations
#10		t_addr=0; t_data=1;
#10		t_addr=1; t_data=2;
#10		t_addr=2; t_data=8;
#10		t_addr=3; t_data=32;

//Read all memory locations
#10	t_we=0;  t_addr=0;
#10		 t_addr=1;
#10		 t_addr=2;
#10		 t_addr=3;

//Perform a reset and check data in memory
#10	t_reset=1;  
#10	t_reset=0; t_addr=0;
#10		   t_addr=1;
#10		   t_addr=2;
#10		   t_addr=3;

end

endmodule