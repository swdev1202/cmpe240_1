/*
 * file: subtractor.v
 * It takes two 1 Byte data and simply outputs the subtraction of two.
 */

module subtractor(f, a, b);
    output[7:0] f;
    input[7:0] a, b;

    assign f = a - b; 
endmodule