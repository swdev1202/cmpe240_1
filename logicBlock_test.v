/*
 * file: logicBlock_test.v
 * This is a test bench program to check if the logic block is working properly.
 */
module logicBlock_test;

	// inputs
	reg t_clk, t_reset;
	reg[7:0] t_din;
	// outputs
	wire[7:0] t_dout;
	
//instantiation
logicBlock logicBlockModule(.dout(t_dout),
							.din(t_din),
							.clk(t_clk),
							.reset(t_reset));

initial
begin
	/* initiate */
	t_clk = 0;
	t_reset = 1;
end

always #100 t_clk = ~t_clk;

initial
begin
	// init
	#50 t_reset = 0'b0;
	// A > B ; result = A - B Testing
	// first data = 9, second data = 5; result dout = (9-5) = 4
	#25 t_din = 8'h09;
	#125 t_din = 8'h05;
	// first data = 7, second data = 4; result dout = (7-4) = 3
	#200 t_din = 8'h07;
	#200 t_din = 8'h04;
	// first data = 5, second data = 3; result dout = (5-3) = 2
	#200 t_din = 8'h05;
	#200 t_din = 8'h03;
	// first data = 3, second data = 2; result dout = (3-2) = 1
	#200 t_din = 8'h03;
	#200 t_din = 8'h02;
	// A < B; result = A + B Testing
	// first data = 0, second data = 4; result dout = (0+4) = 4
	#200 t_din = 8'h00;
	#200 t_din = 8'h04;
	// first data = 1, second data = 2; result dout = (1+2) = 3
	#200 t_din = 8'h01;
	#200 t_din = 8'h02;
	// first data = 5, second data = 6; result dout = (5+6) = 11
	#200 t_din = 8'h05;
	#200 t_din = 8'h06;
	// first data = 9, second data = 10; result dout = (9+10) = 19
	#200 t_din = 8'h09;
	#200 t_din = 8'h0a;
	// A = B; result = A + B Testing
	// first data = 3, second data = 3; result dout = (3+3) = 6
	#200 t_din = 8'h03;
	#200 t_din = 8'h03;
	// break point
	#1000 $stop;
end

endmodule