/*
 * File: comparator_test.v
 * This is a test bench file to test comparator's funtions
 */

module comparator_test;

	reg[7:0] d1;
	reg[7:0] d2;
	wire out;
	
// instantiation
comparator comp1(.output_Sign(out),
				 .input_D1(d1),
				 .input_D2(d2));

initial
begin
	d1 = 8'b00000011; d2 = 8'b00000001; // d1 > d2, out = 1
#5  d1 = 8'b00000001; d2 = 8'b00000011; // d1 < d2, out = 0
#5  d1 = 8'b00000011; d2 = 8'b00000011; // d1 = d2, out = 0
#5  d1 = 8'b10000000; d2 = 8'b11111111; // d1 < d2, out = 0
#5  d1 = 8'b11111111; d2 = 8'b10000000; // d1 > d2, out = 1
#5  d1 = 8'b10000001; d2 = 8'b10000001; // d1 = d2, out = 0
end

endmodule