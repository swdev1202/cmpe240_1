/*
 * file: mtm_testbench.v
 * This is the test bench program to test memory to memory transfer
 */

module mtm_testbench;
	// inputs
	reg t_clk, t_reset;
	reg[7:0] t_dataIn;
	// outputs
	wire[7:0] t_MemB_Out;
	
//instantiation
systemBlock systemModule(.dataIn(t_dataIn),
						 .MemB_Out(t_MemB_Out),
						 .s_clk(t_clk),
						 .s_reset(t_reset));
						 
initial
begin
	/* initiate */
	t_clk = 0;
	t_reset = 1;
end

always #100 t_clk = ~t_clk;

initial
begin
	// init	
	#80 t_reset = 1'b0;
	#30 t_dataIn = 8'h0a; // 10
	#200 t_dataIn = 8'h05; // 5
	// A0 > A1
	// expected B[0] = 8'h0a - 8'h05 = 8'h05
	#200 t_dataIn = 8'h03; // 3
	#200 t_dataIn = 8'h0f; // 15
	// A2 < A3
	// expected B[1] = 8'h03 + 8'h0f = 8'h12
	#200 t_dataIn = 8'h02; // 2
	#200 t_dataIn = 8'h02; // 2
	// A4 = A5
	// expected B[2] = 8'h02 + 8'h02 = 8'h04
	#200 t_dataIn = 8'h8f; // -49 
	#200 t_dataIn = 8'h8e; // -50
	// with a negative number...
	// A6 > A7
	// expected B[3] = 8'h8f - 8'h8e = 8'h01
	
	/*
	#150 t_dataIn = 8'h06;
	#200 t_dataIn = 8'h05;
	#200 t_dataIn = 8'h04;
	#200 t_dataIn = 8'h03;
	#200 t_dataIn = 8'h02;
	#200 t_dataIn = 8'h01;
	#200 t_dataIn = 8'h00;*/
end

endmodule