/*
 * file: counter_2bit.v
 * This is a 2-bit counter with a reset signal.
 */
module counter_2bit(out, clk, reset, enable);
output[1:0] out;
input clk, reset, enable;
reg[1:0] out;

initial
begin
	out = 2'b00;
end

always @(posedge clk)
begin
	if(reset)
	begin
		out <= 2'b00;
	end
	else if(enable)
	begin
		out <= out + 1;
	end
end

endmodule