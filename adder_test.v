/*
 * file: adder_test.v
 * This is a test bench program to check if adder is working properly.
 */

module adder_test;

	reg[7:0] d1;
	reg[7:0] d2;
	wire[7:0] out;

// instantiation
adder add(.f(out),
		  .a(d1),
		  .b(d2));

initial
begin
	d1 = 8'b00000001; d2 = 8'b00000001; // 1+1, out=2
#5	d1 = 8'b00001111; d2 = 8'b00010000; // 15+16, out=31
#5	d1 = 8'b10000000; d2 = 8'b00000001; // -128+1, out=-127
#5	d1 = 8'b00000011; d2 = 8'b00001111; // 3+15, out=18
end

endmodule
