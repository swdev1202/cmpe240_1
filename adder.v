/*
 * file: adder.v
 * It takes two 1 Byte data and simply outputs the sum of two.
 */

module adder(f, a, b);
    output[7:0] f;
    input[7:0] a, b;

    assign f = a + b;
endmodule