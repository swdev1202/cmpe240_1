/*
 * File: subtractor_test.v
 * This is a test bench program to check if subtractor is working properly.
 */
 
module subtractor_test;

	reg[7:0] d1;
	reg[7:0] d2;
	wire[7:0] out;

// instantiation
subtractor subtract(.f(out),
					.a(d1),
					.b(d2));

initial
begin
	d1 = 8'b00001111; d2 = 8'b00000011; // 15-3, out = 12
#5	d1 = 8'b11111111; d2 = 8'b11111101; // -1 - (-3), out = 2
#5	d1 = 8'b00000001; d2 = 8'b00000011; // 1 - 3, out = -2
end

endmodule