/*
 * file: ff_test.v
 * This is a test bench program to check if the flip-flop is working properly.
 */
 
module ff_test;

	// inputs
	reg[7:0] t_d;
	reg t_clk, t_reset;
	// outputs
	wire[7:0] t_q;
	
// instantiation
ff flipflop(.q(t_q),
			.d(t_d),
			.clk(t_clk),
			.reset(t_reset));
			

initial
begin
	/* initialize */
	t_d = 0;
	t_clk = 0;
	t_reset = 0;
end

always #100 t_clk = ~t_clk;

initial
begin
	#100 t_reset = 0'b1;
	#100 t_reset = 0'b0;
	#50 t_d = 8'b11110000;
	#200 t_d = 8'b11111111;
	#200 t_d = 8'b00000001;
	#200 t_d = 8'b00000011;
	#400 $stop;
end

endmodule