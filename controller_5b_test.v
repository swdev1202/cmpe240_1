/* 
 * file: controller_5b_test.v
 * This the 5-bit Up-Counter controller test bench
 */

module controller_5b_test;

reg t_clk, t_reset;
wire t_incA, t_incB, t_weA, t_weB;

controller_5b test(.c_incA(t_incA),
				.c_incB(t_incB),
				.c_weA(t_weA),
				.c_weB(t_weB),
				.c_clk(t_clk),
				.c_reset(t_reset));

initial
begin
  t_clk = 0;
  t_reset = 1;
end

always #100 t_clk = ~t_clk;

initial
begin

#50 t_reset = 0;

end

endmodule
