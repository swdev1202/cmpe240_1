/*
 * file: counter_3bit.v
 * This is a 3-bit counter with a reset signal.
 */
module counter_3bit(out, clk, reset, enable);
output[2:0] out;
input clk, reset, enable;
reg[2:0] out;

initial
begin
	out = 3'b000;
end

always @(posedge clk)
begin
	if(reset)
	begin
		out <= 3'b000;
	end
	else if(enable)
	begin
		out <= out + 1;
	end
end

endmodule