/*
 * file: mux2_1_test.v
 * This is a test bench program to check if 2-to-1 mux is working properly.
 */
 
module mux2_1_test;

	reg[7:0] t_a;
	reg[7:0] t_b;
	reg t_sel;
	wire[7:0] t_out;
	
// instantiation
mux2_1 mux1(.out(t_out),
			.sel(t_sel),
			.a(t_a),
			.b(t_b));
			
initial
begin
	t_sel = 0; t_a = 8'h01; t_b = 8'h02; // (!t_sel) -> out = t_a = 8'h01
#5	t_sel = 1; t_a = 8'h03; t_b = 8'hff; // (t_sel) -> out = t_b = 8'hff
end

endmodule