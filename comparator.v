/*
 * file: comparator.v
 * It takes two inputs and compare the first input to the second input
 * If the first input is bigger than the second input, it outputs 1
 * Otherwise, 0.
 */

module comparator(output_Sign, input_D1, input_D2);

output output_Sign;
input[7:0] input_D1;
input[7:0] input_D2;

assign output_Sign = (input_D1 > input_D2) ? 1:0;

endmodule