/* 
 * file: controller_5b.v
 * This the 5-bit Up-Counter controller
 */
module controller_5b(c_incA, c_incB, c_weA, c_weB, c_clk, c_reset);
 
output c_incA, c_incB, c_weA, c_weB;
input c_clk, c_reset;

reg [4:0] counter;

/* we have to initialize the counter before we even use */
initial
begin
	counter = 5'b00000;
end

always @ (posedge c_clk)
begin
	if(c_reset)
	begin
		counter <= 5'b00000;
	end
	else if(counter == 5'b10010)
	begin
		counter <= 5'b00000;
	end
	else
	begin
		counter <= counter + 1;
	end
end

//Defines when c_weA turns on
assign c_weA =  (counter == 5'b00001) ? 1:
		(counter == 5'b00010) ? 1:
		(counter == 5'b00011) ? 1:
		(counter == 5'b00100) ? 1:
		(counter == 5'b00101) ? 1:
		(counter == 5'b00110) ? 1:
		(counter == 5'b00111) ? 1:
		(counter == 5'b01000) ? 1:
					0;

//Defines when c_weB turns on
assign c_weB = 	(counter == 5'b01011) ? 1:
		(counter == 5'b01101) ? 1:
		(counter == 5'b01111) ? 1:
		(counter == 5'b10001) ? 1:
					0;

//Defines when incA turns on
assign c_incA = (counter == 5'b00000) ? 0:
		(counter == 5'b10001) ? 0:
		(counter == 5'b10010) ? 0:
		(counter == 5'b10011) ? 0:
					1;

//Defines when incB turns on
assign c_incB =	(counter == 5'b01100) ? 1:
		(counter == 5'b01110) ? 1:
		(counter == 5'b10000) ? 1:
		(counter == 5'b10010) ? 1:
					0;

endmodule
  

