/*
 * file: counter_2bit_test.v
 * This is a test bench program to check if the 2-bit counter is working properly.
 */
 
module counter_2bit_test;

	// inputs
	reg t_clk, t_reset, t_enable;
	// outputs
	wire[1:0] t_out;
	
// instantiation
counter_2bit counter(.out(t_out),
					 .clk (t_clk),
					 .reset (t_reset),
					 .enable (t_enable));

initial
begin
	/* initialize */
	t_clk = 0;
	t_reset = 0;
	t_enable = 0;
end

always #100 t_clk = ~t_clk;

initial
begin
	#100 t_reset = 0'b1;
	#100 t_reset = 0'b0;
	#100 t_enable = 0'b1;
	#900 t_reset = 0'b1;
	#500 $stop;
end

endmodule