/*
 * File: mux2_1.v
 * This is a 2-to-1 mux which outputs depending on the select bit
 */

module mux2_1(out, sel, a, b);
    // output ports
	output[7:0] out;
	// input ports
    input sel;
    input[7:0] a,b;
	// internal variables
	wire[7:0] out;

    assign out = sel? b : a;
	
endmodule