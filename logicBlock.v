/* This logic block is in between 8B MEM and 4B MEM */
module logicBlock(dout, din, clk, reset);
	output [7:0] dout;
	input [7:0] din;
	input clk, reset;

	wire[7:0] ff_out;
	wire select;
	wire[7:0] add_out;
	wire[7:0] sub_out;

ff flip_flop(.q(ff_out),
			 .d(din),
			 .clk(clk),
			 .reset(reset));

comparator comp(.output_Sign(select),
				.input_D2(ff_out),
				.input_D1(din));

adder add(.f(add_out),
		  .a(ff_out),
		  .b(din));

subtractor sub(.f(sub_out),
			   .a(ff_out),
			   .b(din));
			   
mux2_1 mux(.out(dout),
		   .sel(select),
		   .a(sub_out),
		   .b(add_out));
		   
endmodule