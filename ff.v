/*
 * file: ff.v
 * This is a positive edge triggered 8-bit flip flop with a reset signal.
 */

module ff(q, d, clk, reset);
output[7:0] q;
input[7:0] d;
input clk, reset;
reg[7:0] q;

always @(posedge clk)
begin
	if(reset)
	begin
	    q <= 0;
	end
	else
	begin
	    q <= d;
	end
end

endmodule