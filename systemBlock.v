/* 
 * file: systemBlock.v
 * This block ties Counter A and Counter B to logic and memory blocks.
 */

module systemBlock(dataIn, MemB_Out, s_clk, s_reset);
input s_clk, s_reset;
input [7:0] dataIn;
output [7:0] MemB_Out;

wire [2:0] addrA;	//CounterA to MemoryA
wire [1:0] addrB;	//CounterB to MemoryB
wire [7:0] DOut1;	//Connects MemoryA to Logic Block
wire [7:0] DataInB;	//Connects MemoryB to Logic Block
wire s_weA, s_weB, s_incA, s_incB;

controller_5b controller(.c_incA(s_incA),
			 .c_incB(s_incB),
			 .c_weA(s_weA),
			 .c_weB(s_weB),
			 .c_clk(s_clk),
			 .c_reset(s_reset));

counter_3bit counterA(.out(addrA),
			.clk(s_clk),
			.reset(s_reset),
			.enable(s_incA));

counter_2bit counterB(.out(addrB),
			.clk(s_clk),
			.reset(s_reset),
			.enable(s_incB));

memory_8b memoryA(.dout(DOut1),
			.din(dataIn),
			.addr(addrA),
			.clk(s_clk),
			.we(s_weA),
			.reset(s_reset));

memory_4b memoryB(.dout(MemB_Out),
			.din(DataInB),
			.addr(addrB),
			.clk(s_clk),
			.we(s_weB),
			.reset(s_reset));

logicBlock logicB(.dout(DataInB),
			.din(DOut1),
			.clk(s_clk),
			.reset(s_reset));

endmodule
			
