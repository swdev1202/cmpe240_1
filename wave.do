onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clock /mtm_testbench/t_clk
add wave -noupdate -label Reset /mtm_testbench/t_reset
add wave -noupdate -label AddrA /mtm_testbench/systemModule/memoryA/addr
add wave -noupdate -label DataInA /mtm_testbench/t_dataIn
add wave -noupdate -label DOut1 /mtm_testbench/systemModule/logicB/din
add wave -noupdate -label DOut2 /mtm_testbench/systemModule/logicB/ff_out
add wave -noupdate -label ADDOut /mtm_testbench/systemModule/logicB/add_out
add wave -noupdate -label SUBOut /mtm_testbench/systemModule/logicB/sub_out
add wave -noupdate -label AddrB /mtm_testbench/systemModule/memoryB/addr
add wave -noupdate -label WEA /mtm_testbench/systemModule/controller/c_weA
add wave -noupdate -label IncA /mtm_testbench/systemModule/controller/c_incA
add wave -noupdate -label WEB /mtm_testbench/systemModule/controller/c_weB
add wave -noupdate -label IncB /mtm_testbench/systemModule/controller/c_incB
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3721 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 318
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1324 ns} {4915 ns}
